/* File:        commanddecoding.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file determines whether commands received from UART0
 *              are valid. Extracts command arguments if valid.
 *
 * Last Edited: 15 October 2019
 */


/* Included Headers */
#include "commandprocessing.h"
#include "queue.h"
#include <ctype.h>
#include <string.h>
#include "structures.h"
#include "utility.h"


/* Number of command supported by UART command interface */
#define NUM_COMMANDS (3U)

/* Definition of number of maximum fields present in time/date arguments */
#define MAX_ARG_FIELDS (3U)

/* Definition of ASCII Character '0' */
#define ZEROCHAR ('0')

/* Definition of numerical base used when converting ASCII character sets
 * to an integer interpretation.
 */
#define ASCIICHAR_BASE (10U)


/* Prototypes for functions to be referenced in a following variable declaration */
void decodeTime(unsigned char index, char * argument);
void decodeDate(unsigned char index, char * argument);

/* Definition of delimiters used to separate command keyword from
 * its arguments as well as fields within an argument.
 */
const char * const COMMAND_DELIMITER = " ";
const char * const TIME_DELIMITER = ":";
const char * const DATE_DELIMITER = "-";


/* Structure: command
 *
 * Description: Data structure containing information used to identify a
 *              valid command and call its corresponding routine.
 */
struct command
{
    char * keyword;
    void (*decode_func)(unsigned char, char *);
    void (*cmd_func)(unsigned int *, unsigned char);
};

/* Table of information about supported commands */
struct command command_table[NUM_COMMANDS] = {{"time",  decodeTime, commandTime},
                                              {"date",  decodeDate, commandDate},
                                              {"alarm", decodeTime, commandAlarm}};


/* Function: ASCIISettoDecimal
 *
 * Description: Converts an ASCII set of characters to a base 10 integer.
 *
 * Arguments:   set - set of characters to be interpreted as an integer
 *
 * Returns:     unsigned int - integer interpretation of input character set
 */
unsigned int ASCIISettoDecimal(char * set)
{
    unsigned int result = CLEAR;
    char * digit = set;

    /* Add each character in set to resulting value */
    while(*digit != NUL)
    {
        result = (result * ASCIICHAR_BASE) + (*digit - ZEROCHAR);
        digit++;
    }

    return result;
}


/* Function: decodeTime
 *
 * Description: Determines arguments/fields passed with command
 *
 * Arguments:   index    - index of command whose argument is being decoded
 *              argument - argument string received
 *
 * Returns:     void
 */
void decodeTime(unsigned char index, char * argument)
{
    char * field;
    unsigned int int_fields[MAX_ARG_FIELDS] = {CLEAR};
    unsigned char num_fields = CLEAR;

    /* Check if argument has any fields */
    if(argument != NULL)
    {
        /* Check for leading delimiter (":") character */
        if(argument[num_fields] == *TIME_DELIMITER)
        {
            /* Argument leads with a delimiter so hours are assumed to be zero */
            int_fields[num_fields] = CLEAR;
            num_fields++;

            /* Check for second delimiter character at the start of the argument */
            if(argument[num_fields] == *TIME_DELIMITER)
            {
                /* Argument leads with two delimiters so minutes are also assumed to be zero */
                int_fields[num_fields] = CLEAR;
                num_fields++;
            }

            /* If too many fields are found after this point, it will be caught and rejected by the following parsing code */
        }

        /* Get the first field from argument */
        field = strtok(argument, TIME_DELIMITER);

        /* Convert and obtain all remaining fields from argument */
        while((field != NULL) &&
              (num_fields < MAX_ARG_FIELDS))
        {
            /* Obtain integer value from argument field string */
            int_fields[num_fields] = ASCIISettoDecimal(field);

            /* Increment number of fields */
            num_fields++;

            /* Search argument for next field */
            field = strtok(NULL, TIME_DELIMITER);
        }
    }

    /* Check that the maximum number of fields has not been exceeded */
    if(num_fields > MAX_ARG_FIELDS)
    {
        /* Must reject command since too many argument fields were given */
        REJECT_COMMAND;
    }
    else
    {
        /* Check integer values to make sure they are valid for their respective time entries */
        if((int_fields[0U] > MAX_HOURS)   ||
           (int_fields[1U] > MAX_MINUTES) ||
           (int_fields[2U] > MAX_SECONDS))
        {
            /* Invalid field was found so reject command */
            REJECT_COMMAND;
        }
        else
        {
            /* Carry out command */
            command_table[index].cmd_func(int_fields, num_fields);
        }
    }

    return;
}


/* Function: decodeDate
 *
 * Description: Determines arguments/fields passed with command
 *
 * Arguments:   index    - index of command whose argument is being decoded
 *              argument - argument string received
 *
 * Returns:     void
 */
void decodeDate(unsigned char index, char * argument)
{
    char * field;
    unsigned int int_fields[MAX_ARG_FIELDS] = {CLEAR};
    unsigned char num_fields = CLEAR;
    unsigned char num_month = CLEAR;

    /* Check if argument has any fields */
    if(argument != NULL)
    {
        /* Start by getting the first field from argument */
        field = strtok(argument, DATE_DELIMITER);

        /* Convert and obtain all remaining fields from argument */
        while(num_fields < MAX_ARG_FIELDS)
        {
            /* Check whether expected field is a number or name string */
            if(num_fields != SET)
            {
                /* Obtain integer value from argument field string */
                int_fields[num_fields] = ASCIISettoDecimal(field);
            }
            else
            {
                /* Convert entire month name string to lowercase */
                for(num_month = CLEAR; field[num_month] != NUL; num_month++)
                {
                    field[num_month] = tolower(field[num_month]);
                }

                /* Clear month tracking iterator */
                num_month = CLEAR;

                /* Obtain month value from name string */
                while(num_month < MAX_MONTHS)
                {
                    /* Search month name array for a match with the provided string */
                    if(strcmp(MONTH_NAMES[num_month], field) == CLEAR)
                    {
                        /* Match was found so break from search loop */
                        break;
                    }

                    /* Increment month to be searched */
                    num_month++;
                }

                /* Check whether a match was found in the loop */
                if(num_month != MAX_MONTHS)
                {
                    /* Match was found so assign month's integer value to field */
                    int_fields[num_fields] = num_month;
                }
                else
                {
                    /* No match was found so must reject command and abort execution */
                    REJECT_COMMAND;
                    return;
                }
            }

            /* Increment number of fields */
            num_fields++;

            /* Search argument for next field */
            field = strtok(NULL, DATE_DELIMITER);
        }
    }
    else
    {
        /* Do nothing since there are no fields to obtain */
    }

    /* Check that the correct number of fields was given */
    if((num_fields != MAX_ARG_FIELDS) && (num_fields != CLEAR))
    {
        /* Must reject command since too many argument fields were given */
        REJECT_COMMAND;
    }
    /* Check day and year inputs taking the leap year into account */
    else if((int_fields[1U] == LEAP_YEAR_MONTH) &&
            ((int_fields[0U] > (MAX_DAYS[int_fields[1U]] + isLeapYear(int_fields[2U]))) ||
             (int_fields[2U] > MAX_YEARS)))
    {
        /* Invalid field was found so reject command */
        REJECT_COMMAND;
    }
    /* Check day and month inputs without taking the leap year into account */
    else if((int_fields[1U] != LEAP_YEAR_MONTH) &&
            ((int_fields[0U] > MAX_DAYS[int_fields[1U]]) ||
             (int_fields[2U] > MAX_YEARS)))
    {
        /* Invalid field was found so reject command */
        REJECT_COMMAND;
    }
    else
    {
        /* Decode argument string per the appropriate command */
        command_table[index].cmd_func(int_fields, num_fields);
    }

    return;
}


/* Function: decodeCommand
 *
 * Description: Determines which command has been received from user
 *
 * Arguments:   command - command string received
 *
 * Returns:     void
 */
void decodeCommand(char * command)
{
    char * keyword;
    int result;
    unsigned char i = CLEAR;
    char * argument;

    /* Obtain command keyword from provided line */
    keyword = strtok(command, COMMAND_DELIMITER);

    /* Convert entire keyword to lowercase */
    for(i = CLEAR; keyword[i] != NUL; i++)
    {
        keyword[i] = tolower(keyword[i]);
    }

    /* Search received command string for valid command keyword */
    for(i = CLEAR; i < NUM_COMMANDS; i++)
    {
        result = strcmp(keyword, command_table[i].keyword);

        /* Check for a match */
        if(result == CLEAR)
        {
            /* Found a matching keyword so stop searching */
            break;
        }
    }

    /* Check that a match was found in the search */
    if(i == NUM_COMMANDS)
    {
        /* No match was found so print "?\n" to console */
        REJECT_COMMAND;
    }
    else
    {
        /* Keyword match was found so look for argument by searching
         * for a token after the command's initial keyword.
         */
        argument = strtok(NULL, COMMAND_DELIMITER);

        /* Then check that there are no tokens after the argument */
        if(strtok(NULL, COMMAND_DELIMITER) == NULL)
        {
            /* No token was found after the argument (if there is one)
             * so decode argument.
             */
            command_table[i].decode_func(i, argument);
        }
        else
        {
            /* Extra token was found so reject command */
            REJECT_COMMAND;
        }
    }

    return;
}


