/* File:        queue.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains prototypes of functions used in
 *              queue.c that are also called elsewhere in the project.
 *
 * Last Edited: 30 September 2019
 */

/* Wrapper ifdef */
#ifndef SRC_QUEUE_H_
#define SRC_QUEUE_H_


/* Macro definitions that are useful in more than one other source file used
 * to print a command rejection message through UART0.
 */
#define REJECT_COMMAND (enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, "?\n"))


/* Enum: Queues
 *
 * Description: Enumeration of queues that can be added to/taken from.
 */
enum Queues
{
    QUEUE_COMMANDS = 0U,
    QUEUE_FEEDBACK = 1U
};

/* Enum: Sources
 *
 * Description: Enumeration of sources from which queue entries can originate.
 */
enum Sources
{
    SOURCE_UART       = 0U,
    SOURCE_SYSTICK    = 1U,
    SOURCE_DIAGNOSTIC = 2U
};


/* Function: initQueues
 *
 * Description: Initializes queues to a default safe state
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void initQueues(void);


/* Function: enqueue
 *
 * Description: Adds a word to the desired queue.
 *
 * Arguments:   queue  - indicator of which queue is to be added to
 *              source - indicator of new entry's origin
 *              entry  - entry character to be added to queue
 *
 * Returns:     void
 */
void enqueue(enum Queues queue, enum Sources source, char entry);


/* Function: enqueueString
 *
 * Description: Wrapper function that uses enqueue() to add an entire
 *              string to the desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *              source - indicator of new entry's origin
 *              entry - entry string to be added to queue. Is assumed to
 *                      be null-terminated.
 *
 * Returns:     void
 */
void enqueueString(enum Queues queue, enum Sources source, char * const entry);


/* Function: dequeue
 *
 * Description: Removes an entry character from the desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *
 * Returns:     unsigned short  - queue entry taken from front of queue.
 *                                If queue is empty, a value of zero is returned by default.
 */
unsigned short dequeue(enum Queues queue);


/* Function: checkQueueSize
 *
 * Description: Checks size of desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *
 * Returns:     unsigned short - number of entries in desired queue
 */
unsigned short checkQueueSize(enum Queues queue);


/* Wrapper endif */
#endif /* SRC_QUEUE_H_ */
