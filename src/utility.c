/* File:        utility.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file defines generic useful functions.
 *
 * Last Edited: 8 October 2019
 */


/* Included Headers */
#include "utility.h"


/* Definition of values used to determine whether a given year is a leap year */
#define LEAP_YEAR_DIVISOR (4U)
#define CENTURY (100U)


/* Function:    isLeapYear
 *
 * Description: This function will determine whether is input value corresponds
 *              to a Gregorian leap year
 *
 * Arguments:   yr - year to be tested for a leap year
 *
 * Returns:     unsigned char - indicator of whether input year value is a leap year,
 *                              1->is a leap year, 0->o/w
 */
unsigned char isLeapYear(unsigned short yr)
{
    unsigned char result = CLEAR;

    /* Check whether the provided year is divisible by the leap year divisor */
    if((yr % LEAP_YEAR_DIVISOR) == CLEAR)
    {
        /* Must also check that the year is not a century non-divisible by 400 */
        if(((yr % CENTURY) != CLEAR) || ((yr % (LEAP_YEAR_DIVISOR * CENTURY)) == CLEAR))
        {
            /* Provided year is a leap year so adjust result */
            result = SET;
        }
    }

    return result;
}
