/* File:        structures.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains definitions and instances of the data
 *              structures used to store information accessible to a user.
 *
 * Last Edited: 7 October 2019
 */

/* Wrapper ifdef */
#ifndef SRC_STRUCTURES_H_
#define SRC_STRUCTURES_H_


/* Definitions of the date/time fields' maximum values. Note that an array 12 bytes in length
 * was used for the maximum number of days in each month. While this results in a slightly
 * larger memory usage, access speed is very high as there is no need for logic to determine
 * which value to use as a month's length in days.
 */
#define MAX_TENTHS  (9U)
#define MAX_SECONDS (59U)
#define MAX_MINUTES (59U)
#define MAX_HOURS   (23U)

#define MAX_MONTHS  (12U)
#define MAX_YEARS   (9999U)

/* Definition of index of month that gains a day in a leap year */
#define LEAP_YEAR_MONTH (1U)
                                                 /*  J    F    M    A    M    J    J    A    S    O    N    D */
static const unsigned char MAX_DAYS[MAX_MONTHS] = {31U, 28U, 31U, 30U, 31U, 30U, 31U, 31U, 30U, 31U, 30U, 31U};

/* Array of keywords associated with each possible value of date.month */
static const char * const MONTH_NAMES[MAX_MONTHS] = {"jan", "feb", "mar", "apr", "may", "jun",
                                                     "jul", "aug", "sep", "oct", "nov", "dec"};


/* Structure: time
 *
 * Description: The time data structure holds a valid time of day with a resolution
 *              of one tenth of a second. Note that when setting the time, it can only
 *              be set to a resolution of one second.
 */
struct time
{
    unsigned char hour;
    unsigned char minute;
    unsigned char second;
    unsigned char tenth_second;
};


/* Structure: date
 *
 * Description: The time date structure holds a valid day in the form
 *              Day/Month/Year.
 */
struct date
{
    unsigned char day;
    unsigned char month;
    unsigned short year;
};


/* Time variable that can be set/queried using commands.
 * This value starts at 00:00:00.0.
 */
extern struct time time;

/* Date variable that can be set/queried using commands.
 * This value starts at 1/Sept/2019. Note that the month index here
 * is actually one less than the month's index in the calendar year.
 */
extern struct date date;

/* Alarm time variable used to trigger alarm set by commands.
 * This value starts at 00:00:00.0.
 */
extern struct time alarm_time;

/* Flag used to determine whether alarm is active. Alarm will be considered
 * active if this flag has a value of 1 and inactive if this flag has a value
 * of 0.
 */
extern unsigned char alarm_active;

/* Flag used to determine whether current year is a leap year. A value of 1
 * indicates a current leap year while a value of 0 indicates otherwise.
 */
extern unsigned char leap_year;


/* Wrapper endif */
#endif /* SRC_STRUCTURES_H_ */
