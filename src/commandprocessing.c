/* File:        commandprocessing.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file carries out valid commands received from UART0.
 *
 * Last Edited: 7 October 2019
 */

/* Included Headers */
#include "queue.h"
#include "structures.h"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "utility.h"


/* Definition of buffer size for command output strings */
#define MAX_OUT_LEN (80U)


/* Function: commandTime
 *
 * Description: Sets/queries current time.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandTime(unsigned int * arg, unsigned char num_args)
{
    char out_str[MAX_OUT_LEN];

    /* Determine which fields were given based on how many were included in command */
    switch(num_args)
    {
    /* Nothing was given */
    case 0U:
        /* Do not change current time */
        break;
    /* Only hours was given */
    case 1U:
        /* Apply provided field and zero the rest */
        time.hour = arg[0U];
        time.minute = CLEAR;
        time.second = CLEAR;
        time.tenth_second = CLEAR;
        break;

    /* Hours and minutes were given */
    case 2U:
        /* Apply provided fields and zero the rest */
        time.hour = arg[0U];
        time.minute = arg[1U];
        time.second = CLEAR;
        time.tenth_second = CLEAR;
        break;

    /* Hours, minutes and seconds were given. This includes cases
     * such as an entered argument of ::15 indicating a time of
     * 00:00:15.
     */
    case 3U:
        /* Apply provided fields and zero the rest */
        time.hour = arg[0U];
        time.minute = arg[1U];
        time.second = arg[2U];
        time.tenth_second = CLEAR;
        break;

    /* This case should never be entered so simply reject command and move on */
    default:
        REJECT_COMMAND;
        return;
    }

    /* Print currently stored time to console */
    sprintf(out_str, "%u:%u:%u.%u\n", time.hour, time.minute, time.second, time.tenth_second);
    enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, out_str);

    return;
}


/* Function: commandDate
 *
 * Description: Sets/queries current date.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandDate(unsigned int * arg, unsigned char num_args)
{
    char out_str[MAX_OUT_LEN];

    /* Determine which fields were given based on how many were included in command */
    switch(num_args)
    {
    /* Nothing was given */
    case 0U:
        /* Do not change current date */
        break;
    /* All date fields must be given for command to modify the current date */
    case 3U:
        /* Apply provided fields and zero the rest */
        date.day = arg[0U];
        date.month = arg[1U];
        date.year = arg[2U];

        /* Adjust leap year tracker */
        leap_year = isLeapYear(date.year);
        break;
    /* This case should never be entered so simply reject command and move on */
    default:
        REJECT_COMMAND;
        return;
    }

    /* Print currently stored time to console */
    sprintf(out_str, "%u-%s-%u\n", date.day, MONTH_NAMES[date.month], date.year);
    enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, out_str);

    return;
}


/* Function: commandAlarm
 *
 * Description: Sets/clears current alarm.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandAlarm(unsigned int * arg, unsigned char num_args)
{
    char out_str[MAX_OUT_LEN];

    /* Determine which fields were given based on how many were included in command */
    switch(num_args)
    {
    /* Nothing was given so clear alarm time */
    case 0U:
        alarm_time.hour = CLEAR;
        alarm_time.minute = CLEAR;
        alarm_time.second = CLEAR;
        alarm_time.tenth_second = CLEAR;
        alarm_active = CLEAR;
        enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, "Alarm Cleared: ");
        break;
    /* Only hours was given */
    case 1U:
        /* Apply provided field and zero the rest */
        alarm_time.hour = arg[0U] + time.hour;
        alarm_time.minute = time.minute;
        alarm_time.second = time.second;
        alarm_time.tenth_second = time.tenth_second;
        alarm_active = 1U;
        break;

    /* Hours and minutes were given */
    case 2U:
        /* Apply provided fields and zero the rest */
        alarm_time.hour = arg[0U] + time.hour;
        alarm_time.minute = arg[1U] + time.minute;
        alarm_time.second = time.second;
        alarm_time.tenth_second = time.tenth_second;
        alarm_active = 1U;
        break;

    /* Hours, minutes and seconds were given. This includes cases
     * such as an entered argument of ::15 indicating a time of
     * 00:00:15.
     */
    case 3U:
        /* Apply provided fields and zero the rest */
        alarm_time.hour = arg[0U] + time.hour;
        alarm_time.minute = arg[1U] + time.minute;
        alarm_time.second = arg[2U] + time.second;
        alarm_time.tenth_second = time.tenth_second;
        alarm_active = 1U;
        break;

    /* This case should never be entered so simply reject command and move on */
    default:
        REJECT_COMMAND;
        break;
    }

    /* Check whether alarm time must roll over to next second */
    if(alarm_time.tenth_second > MAX_TENTHS)
    {
        /* Roll alarm time over to next second */
        alarm_time.second += 1U;

        /* Subtract a second's worth from tenths of a second */
        alarm_time.tenth_second -= (MAX_TENTHS + 1U);
    }

    /* Check whether alarm time must roll over to next minute */
    if(alarm_time.second > MAX_SECONDS)
    {
        /* Roll alarm time over to next minute */
        alarm_time.minute += 1U;

        /* Subtract a minute's worth from seconds */
        alarm_time.second -= (MAX_SECONDS + 1U);
    }

    /* Check whether alarm time must roll over to next hour */
    if(alarm_time.minute > MAX_MINUTES)
    {
        /* Roll alarm time over to next hour */
        alarm_time.hour += 1U;

        /* Subtract an hour's worth from minutes */
        alarm_time.minute -= (MAX_MINUTES - 1U);
    }

    /* Check whether alarm time must roll over to start of day */
    if(alarm_time.hour > MAX_HOURS)
    {
        /* Subtract a day's worth from hours */
        alarm_time.hour -= (MAX_HOURS + 1U);
    }

    /* Print currently stored time to console */
    sprintf(out_str, "%u:%u:%u.%u\n", alarm_time.hour, alarm_time.minute, alarm_time.second, alarm_time.tenth_second);
    enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, out_str);

    return;
}
