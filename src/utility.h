/* File:        utility.h
 *
 * Author:      Patrick Wells
 *
 * Description: Contains generic definitions and function prototypes that
 *              could be useful to many difference sources.
 *
 * Last Edited: 8 October 2019
 */

/* Wrapper ifdef */
#ifndef SRC_UTILITY_H_
#define SRC_UTILITY_H_


/* Definition of values that can be used as boolean flag indicators
 * as well as for clearing/setting variables to a safe state.
 */
#define CLEAR (0U)
#define SET (1U)

/* Definitions useful for bit/byte manipulation */
#define BYTE (8U)
#define LSBYTE_SET (0xFFU)
#define SHORT_SET (0xFFFFU)

/* Definition of NUL character */
#define NUL ('\0')

/* Definition of macros used for entering/exiting critical sections */
#define ENTER_CRITICAL_SECTION __asm(" cpsid   i")
#define EXIT_CRITICAL_SECTION __asm(" cpsie   i")


/* Function:    isLeapYear
 *
 * Description: This function will determine whether input value corresponds
 *              to a Gregorian leap year
 *
 * Arguments:   yr - year to be tested for a leap year
 *
 * Returns:     unsigned char - indicator of whether input year value is a leap year,
 *                              1->is a leap year, 0->o/w
 */
unsigned char isLeapYear(unsigned short yr);



/* Wrapper endif */
#endif /* SRC_UTILITY_H_ */
