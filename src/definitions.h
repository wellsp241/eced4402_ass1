/* File:        definitions.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains useful macros that are used
 *              during RTOS startup.
 *
 * Last Edited: 8 October 2019
 */

/* Wrapper ifdef */
#ifndef SRC_DEFINITIONS_H_
#define SRC_DEFINITIONS_H_

/***** SysTick Definitions *****/
/* Register Addresses */
#define REG_ST_CTL    (*((unsigned long *)0xE000E010))
#define REG_ST_RELOAD (*((unsigned long *)0xE000E014))

/* Control/Status Bit Locations */
#define ST_CTL_COUNT      (0x00010000UL)
#define ST_CTL_CLK_SRC    (0x00000004UL)
#define ST_CTL_INTEN      (0x00000002UL)
#define ST_CTL_ENABLE     (0x00000001UL)

/* Values */
/* This reload period was determined from the system's default clock rate of 16MHz.
 * To have this correspond to 1/10 of a second, must set this value to 1 600 000 - 1.
 */
#define ST_RELOAD_PERIOD  (0x001869FFUL)

/***** Clock Gating Definitions *****/
/* Register Addresses */
#define REG_SYSCTL_RCGCGPIO      (*((unsigned long *)0x400FE608))
#define REG_SYSCTL_RCGCUART      (*((unsigned long *)0x400FE618))

/* Control/Status Bit Masks */
#define SYSCTL_RCGCUART_UART0      (0x00000001UL)
#define SYSCTL_RCGCGPIO_GPIOA      (0x00000001UL)

/***** UART Definitions *****/
/* Register Addresses */
#define REG_UART0_CTL         (*((unsigned long *)0x4000C030))
#define REG_UART0_IBRD        (*((unsigned long *)0x4000C024))
#define REG_UART0_FBRD        (*((unsigned long *)0x4000C028))
#define REG_UART0_LCRH        (*((unsigned long *)0x4000C02C))
#define REG_UART0_IM          (*((unsigned long *)0x4000C038))
#define REG_UART0_MIS         (*((unsigned long *)0x4000C040))
#define REG_UART0_DR          (*((unsigned long *)0x4000C000))
#define REG_UART0_ICR         (*((unsigned long *)0x4000C044))
#define REG_UART0_FR          (*((unsigned long *)0x4000C018))

/* Control/Status Bit Masks */
#define UART_CTL_UARTEN         (0x00000301UL)
#define UART_LCRH_WLEN_8        (0x00000060UL)
#define UART_LCRH_FEN           (0x00000010UL)
#define UART_INT_TX             (0x00000020UL)
#define UART_INT_RX             (0x00000010UL)
#define UART_FR_BUSY            (0x00000008UL)

/***** GPIO Definitions *****/
/* Register Addresses */
#define REG_GPIO_PORTA_AFSEL  (*((unsigned long *)0x40058420))
#define REG_GPIO_PORTA_DEN    (*((unsigned long *)0x4005851C))
#define REG_GPIO_PORTA_PCTL   (*((unsigned long *)0x4005852C))

/* Control/Status Bit Masks */
#define GPIO_PORTA_AFSEL_PA0  (0x00000001UL)
#define GPIO_PORTA_AFSEL_PA1  (0x00000002UL)
#define GPIO_PORTA_DEN_PA0    (0x00000001UL)
#define GPIO_PORTA_DEN_PA1    (0x00000002UL)
#define GPIO_PORTA_PCTL_PA0   (0x00000001UL)
#define GPIO_PORTA_PCTL_PA1   (0x00000010UL)

/***** Interrupt Definitions *****/
/* Register Addresses */
#define REG_NVIC_EN0      (*((unsigned long *)0xE000E100))

/* Control/Status Bit Masks */
#define INT_VEC_UART0     (0x00000020UL)


/* Wrapper endif */
#endif /* SRC_DEFINITIONS_H_ */
