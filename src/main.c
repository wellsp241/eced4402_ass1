/* File:        main.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file contains the entry point of the project software.
 *              Pre-RTOS initialization is carried out here (i.e. hardware + ISR setup).
 *
 * Last Edited: 15 October 2019
 */


/* Included Headers */
#include "commanddecoding.h"
#include "definitions.h"
#include "queue.h"
#include "structures.h"
#include "utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Definition of the maximum memory allocated to a single command string */
#define MAX_CMD_LEN (80U)

/* Definition of special characters */
#define BACKSPACE (127U)
#define RETURN ('\r')
#define BEL (0x07U)

/* Macro definition used to start a new line in terminal */
#define NEWLINE enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, "> ")


/* Function:    initSysTick
 *
 * Description: Prepares SysTick for operation. Its counter will start ticking once
 *              this function ends.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void initSysTick(void)
{
    /* Set interrupt period to every 1/10 of a second */
    REG_ST_RELOAD = ST_RELOAD_PERIOD;

    /* Set counter enable and internal clock source bits in control register */
    REG_ST_CTL |= ST_CTL_CLK_SRC | ST_CTL_ENABLE;

    return;
}


/* Function:    initUART
 *
 * Description: Prepares UART for operation.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void initUART(void)
{

    /* Enable clock gating for UART0 and PORTA */
    REG_SYSCTL_RCGCUART |= SYSCTL_RCGCUART_UART0;
    REG_SYSCTL_RCGCGPIO |= SYSCTL_RCGCGPIO_GPIOA;

    /* Ensure UART0 is disabled before modifying setup parameters */
    REG_UART0_CTL &= ~UART_CTL_UARTEN;

    /* Set UART0 Baud Rate of 115200 bits/sec. The integer and fractional rates
     * are calcuated as:
     *
     * IBRD = 16 000 000 / (16 * 115 200)  = 8.680555555555556 -> 8
     * FBRD = 0.680555555555556 * 64 + 0.5 = 44.05555555555556 -> 44
     */
    REG_UART0_IBRD = 8UL;
    REG_UART0_FBRD = 44UL;

    /* Set UART0 word length to 8 bits and enable line FIFO. Parity
     * if left unused and the number of stop bits is left at 1.
     */
    REG_UART0_LCRH |= UART_LCRH_WLEN_8;

    /* Enable receive and transmit on PORTA0 and PORTA1 */
    REG_GPIO_PORTA_AFSEL |= (GPIO_PORTA_AFSEL_PA0) | (GPIO_PORTA_AFSEL_PA1);

    /* Enable receive and transmit UART RX/TX pins on PORTA0 and PORTA1 */
    REG_GPIO_PORTA_PCTL |= (GPIO_PORTA_PCTL_PA0) | (GPIO_PORTA_PCTL_PA1);

    /* Enable digital I/O on PORTA0 and PORTA1 */
    REG_GPIO_PORTA_DEN |= (GPIO_PORTA_DEN_PA0) | (GPIO_PORTA_DEN_PA1);

    /* Enable UART communications now that all settings have been done */
    REG_UART0_CTL |= UART_CTL_UARTEN;

    return;
}


/* Function:    initInterrupts
 *
 * Description: Enables global interrupts on the processor as well as sets up
 *              interrupts used within RTOS.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void initInterrupts(void)
{
    /* Enable SysTick counter interrupts */
    REG_ST_CTL |= ST_CTL_INTEN;

    /* Enable UART0 interrupts */
    REG_NVIC_EN0 |= INT_VEC_UART0;

    /* Enable transmit and receive interrupts on UART0 */
    REG_UART0_IM |= UART_INT_TX | UART_INT_RX;

    /* Issue assembly instruction that enables global interrupts */
    EXIT_CRITICAL_SECTION;

    return;
}


/* Function:    SysTickHandler
 *
 * Description: ISR to be called once SYSTICK reaches zero.
 *              This should occur every tenth of a second.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void SysTickHandler(void)
{
    /* Add SYSTICK signal to command queue */
    enqueue(QUEUE_COMMANDS, SOURCE_SYSTICK, CLEAR);

    return;
}


/* Function:    UART0_IntHandler
 *
 * Description: ISR to be called once UART0 receives or transmits data.
 *              Queues/Dequeues commands/diagnostics to be sent/received
 *              through UART0.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void UART0_IntHandler(void)
{
    /* Received data */
    if((REG_UART0_MIS & UART_INT_RX) != CLEAR)
    {
        /* Clear receive interrupt */
        REG_UART0_ICR |= UART_INT_RX;

        /* Add character to queue for processing */
        enqueue(QUEUE_COMMANDS, SOURCE_UART, REG_UART0_DR);
    }
    /* Transmitted data */
    else if((REG_UART0_MIS & UART_INT_TX) != CLEAR)
    {
        /* Clear transmit interrupt */
        REG_UART0_ICR |= UART_INT_TX;

        /* Check if feedback queue is empty */
        if(checkQueueSize(QUEUE_FEEDBACK) != CLEAR)
        {
            /* Transmit next character in feedback queue */
            REG_UART0_DR = dequeue(QUEUE_FEEDBACK) & LSBYTE_SET;
        }
    }
    /* Other interrupt */
    else
    {
        /* This scenario should not occur but if it does do nothing */
    }

    return;
}


/* Function:    startFeedbackTX
 *
 * Description: This function will start a UART transfer of command line feedback
 *              only if the dedicated UART is not already busy.
 *
 * Arguments:   ch - character to be transmitted
 *
 * Returns:     unsigned char - indicator of whether character was able to be transmitted
 *                              1 if successful, 0 if UART is busy
 */
unsigned char startFeedbackTX(char ch)
{
    unsigned char result = SET;

    /* Check that UART0 is not currently busy */
    if((REG_UART0_FR & UART_FR_BUSY) == CLEAR)
    {
        /* Can send character through UART0 */
        REG_UART0_DR = ch;
    }
    else
    {
        /* UART is currently busy so adjust returned value accordingly and
         * do not attempt to transmit.
         */
        result = CLEAR;
    }

    return result;
}


/* Function:    main
 *
 * Description: As this function is the project's entry point, initialization routines
 *              are called here prior to running the RTOS.
 *
 * Arguments:   void
 *
 * Returns:     As program execution is considered finished after this function is
 *              completed, this function does not return anything.
 */
void main(void)
{
    char * const newcommand = (char *)malloc(MAX_CMD_LEN);
    unsigned short newinput;
    enum Sources newsource;

    /* Initialize SysTick */
    initSysTick();

    /* Initialize UART0 */
    initUART();

    /* Initialize queues */
    initQueues();

    /* Initialize required interrupts */
    initInterrupts();

    /* Start command interface on an indent in terminal */
    NEWLINE;

    /* Wait indefinitely while processing commands received through UART0.
     * This is the monitor portion of the RTOS for the time being.
     */
    while(SET)
    {
        /* Check whether there is anything in the input queue */
        if(checkQueueSize(QUEUE_COMMANDS) != CLEAR)
        {
            /* Remove entry from input queue */
            newinput = dequeue(QUEUE_COMMANDS);
            newsource = (enum Sources)(newinput >> BYTE);
            newinput = newinput & LSBYTE_SET;

            /* Check for character received from UART */
            if(newsource == SOURCE_UART)
            {
                /* Check for actionable characters */
                if(newinput == BACKSPACE)
                {
                    /* Input was a backspace so remove character from command string if possible */
                    if(strlen(newcommand) != CLEAR)
                    {
                        /* Queue received character for output to console */
                        enqueue(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, newinput);

                        /* Command string contains at least 1 character so exclude last received character from string by
                         * overwriting its last non-null character with the NUL character.
                         */
                        newcommand[strlen(newcommand) - 1U] = NUL;
                    }
                }
                else if(newinput == RETURN)
                {
                    /* Queue received character for output to console */
                    enqueue(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, newinput);

                    /* Input was a return character so decode received command string and carry
                     * out command if possible.
                     */
                    decodeCommand(newcommand);

                    /* Clear command string buffer */
                    memset(newcommand, CLEAR, strlen(newcommand));

                    /* Start new command interface on an indent in terminal */
                    NEWLINE;
                }
                else
                {
                    /* Queue received character for output to console */
                    enqueue(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, newinput);

                    /* No special character was received so add character to command string */
                    strcat(newcommand, (char *)&newinput);
                }
            }
            /* Check for signal received from SYSTICK */
            else if(newsource == SOURCE_SYSTICK)
            {
                /* Queue received signal from SYSTICK interrupt so must increment saved time.
                 * First check whether time must roll over to next second.
                 */
                if(time.tenth_second >= MAX_TENTHS)
                {
                    /* Tenths of a second become zero */
                    time.tenth_second = CLEAR;

                    /* Check whether time must roll over to next minute */
                    if(time.second >= MAX_SECONDS)
                    {
                        /* Second becomes zero */
                        time.second = CLEAR;

                        /* Check whether time must roll over to next hour */
                        if(time.minute >= MAX_MINUTES)
                        {
                            /* Minute becomes zero */
                            time.minute = CLEAR;

                            /* Check whether time must roll over to next day */
                            if(time.hour >= MAX_HOURS)
                            {
                                /* Hour becomes zero */
                                time.hour = CLEAR;

                                /* Check whether date must roll over to next month */
                                if(((date.day >= MAX_DAYS[date.month]) && (date.month != LEAP_YEAR_MONTH)) ||
                                   ((date.day >= (MAX_DAYS[date.month] + leap_year)) && (date.month == LEAP_YEAR_MONTH)))
                                {
                                    /* Day becomes 1 */
                                    date.day = SET;

                                    /* Check whether date must roll over to next year */
                                    if(date.month >= (MAX_MONTHS - 1U))
                                    {
                                        /* Month becomes 0. Remember that the month's index in this code's context
                                         * is one less than its index in the conventional calendar (i.e. 0->jan,
                                         * 1->feb, etc.)
                                         */
                                        date.month = CLEAR;

                                        /* Check whether date must roll over to start of the universe */
                                        if(date.year >= MAX_YEARS)
                                        {
                                            /* Year becomes 0 */
                                            date.year = CLEAR;
                                        }
                                        else
                                        {
                                            /* Date moves to next year */
                                            date.year++;
                                        }

                                        /* Adjust leap year tracker */
                                        leap_year = isLeapYear(date.year);
                                    }
                                    else
                                    {
                                        /* Date moves to next month */
                                        date.month++;
                                    }
                                }
                                else
                                {
                                    /* Date moves to next day */
                                    date.day++;
                                }
                            }
                            else
                            {
                                /* Time moves to next hour */
                                time.hour++;
                            }
                        }
                        else
                        {
                            /* Time moves to next minute */
                            time.minute++;
                        }
                    }
                    else
                    {
                        /* Time moves to next second */
                        time.second++;
                    }
                }
                else
                {
                    /* Time moves to next tenth of a second */
                    time.tenth_second++;
                }

                /* Check whether alarm is currently set */
                if(alarm_active == SET)
                {
                    /* Check whether alarm must go off */
                    if((alarm_time.hour == time.hour) && (alarm_time.minute == time.minute) &&
                       (alarm_time.second == time.second) && (alarm_time.tenth_second == time.tenth_second))
                    {
                        /* Alarm must go off; do this by outputting ASCII BEL character through UART0 */
                        enqueue(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, BEL);
                        enqueueString(QUEUE_FEEDBACK, SOURCE_DIAGNOSTIC, "Alarm!\n");
                        NEWLINE;

                        /* Clear alarm set flag */
                        alarm_active = CLEAR;

                        /* Clear command string buffer in case alarm is triggered while a user is
                         * in the middle of writing a command line.
                         */
                        memset(newcommand, CLEAR, strlen(newcommand));
                    }
                }
            }
        }
    }

	return;
}
