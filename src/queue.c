/* File:        queue.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file contains and manages the two queues used when processing
 *              commands received through UART0.
 *
 * Last Edited: 8 October 2019
 */

/* Included Headers */
#include <stdlib.h>
#include <stdio.h>
#include "main.h"
#include "queue.h"
#include "utility.h"


/* Definition of the queues' maximum number of character entries */
#define QUEUE_CAPACITY (64U)


/* Structure: Queue
 *
 * Description: The Queue data structure acts as a queue of 32-bit values.
 */
struct Queue
{
    unsigned short front;
    unsigned short back;
    unsigned short size;
    unsigned short * array;
};

/* Queue storing commands received through UART */
static struct Queue * command_queue;

/* Queue storing feedback to be sent through UART */
static struct Queue * feedback_queue;


/* Function: initQueues
 *
 * Description: Initializes queues to a default safe state
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void initQueues(void)
{
    /* Allocate memory for queue structures */
    command_queue = (struct Queue *)malloc(sizeof(struct Queue));
    feedback_queue = (struct Queue *)malloc(sizeof(struct Queue));

    /* Check that memory for queue structures was allocated properly */
    if((command_queue == NULL) || (feedback_queue == NULL))
    {
        /* Loop indefinitely to preserve state for debugging */
        while(SET);
    }

    /* Set command queues' members to a default safe state. Note that
     * the queue's back starts at its maximum value so that when it is
     * first increased by one during enqueue-ing, it rolls over to zero.
     */
    command_queue->front = CLEAR;
    command_queue->back = SHORT_SET;
    command_queue->size = CLEAR;

    feedback_queue->front = CLEAR;
    feedback_queue->back = SHORT_SET;
    feedback_queue->size = CLEAR;

    /* Allocate memory for circular arrays */
    command_queue->array = (unsigned short*)malloc(QUEUE_CAPACITY * sizeof(unsigned short));
    feedback_queue->array = (unsigned short*)malloc(QUEUE_CAPACITY * sizeof(unsigned short));

    /* Check that memory for queues' entries was allocated properly */
    if((command_queue->array == NULL) || (feedback_queue->array == NULL))
    {
        /* Loop indefinitely to preserve state for debugging */
        while(SET);
    }

    return;
}


/* Function: enqueue
 *
 * Description: Adds a word to the desired queue.
 *
 * Arguments:   queue  - indicator of which queue is to be added to
 *              source - indicator of new entry's origin
 *              entry  - entry character to be added to queue
 *
 * Returns:     void
 */
void enqueue(enum Queues queue, enum Sources source, char entry)
{
    struct Queue * temp_ptr;

    /* Issue assembly instruction that disables global interrupts */
    ENTER_CRITICAL_SECTION;

    /* Determine queue to be worked on */
    if(queue == QUEUE_COMMANDS)
    {
        /* Command queue */
        temp_ptr = command_queue;
    }
    else if(queue == QUEUE_FEEDBACK)
    {
        /* Feedback queue */
        temp_ptr = feedback_queue;

        /* Before queueing, check to see if provided character can be transmitted */
        if(startFeedbackTX(entry) == SET)
        {
            /* Transmission was successful so no need to add to queue */
            return;
        }
    }
    else
    {
        /* Should not get here so loop indefinitely to preserve
         * system state for debugging.
         */
        while(SET);
    }

    /* Queue is full */
    if(temp_ptr->size == QUEUE_CAPACITY)
    {
        /* Do nothing since no entry can be added to a full queue */
    }
    /* Queue is not full */
    else
    {
        /* Create new queue entry containing desired data */
        temp_ptr->back = (temp_ptr->back + 1U) % QUEUE_CAPACITY;
        temp_ptr->array[temp_ptr->back] = source;
        temp_ptr->array[temp_ptr->back] = (temp_ptr->array[temp_ptr->back] << BYTE) | entry;
        temp_ptr->size++;
    }

    /* Issue assembly instruction that enables global interrupts */
    EXIT_CRITICAL_SECTION;

    return;
}


/* Function: enqueueString
 *
 * Description: Wrapper function that uses enqueue() to add an entire
 *              string to the desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *              source - indicator of new entry's origin
 *              entry - entry string to be added to queue. Is assumed to
 *                      be null-terminated.
 *
 * Returns:     void
 */
void enqueueString(enum Queues queue, enum Sources source, char * const entry)
{
    char * charptr = entry;

    /* Call enqueue() once for each character aside from the null terminator
     * in provided string.
     */
    while(*charptr != NUL)
    {
        /* Add character to queue */
        enqueue(queue, source, *charptr);

        /* Increment iterating pointer */
        charptr++;
    }

    return;
}


/* Function: dequeue
 *
 * Description: Removes an entry character from the desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *
 * Returns:     unsigned short  - queue entry taken from front of queue.
 *                                If queue is empty, a value of zero is returned by default.
 */
unsigned short dequeue(enum Queues queue)
{
    unsigned short taken_word = CLEAR;
    struct Queue * temp_ptr;

    /* Determine queue to be worked on */
    if(queue == QUEUE_COMMANDS)
    {
        temp_ptr = command_queue;
    }
    else if(queue == QUEUE_FEEDBACK)
    {
        temp_ptr = feedback_queue;
    }
    else
    {
        /* Should not get here so loop indefinitely to preserve
         * system state for debugging.
         */
        while(SET);
    }

    /* Issue assembly instruction that disables global interrupts */
    ENTER_CRITICAL_SECTION;

    /* Queue is empty */
    if(temp_ptr->size == CLEAR)
    {
        /* Do nothing since no element can be taken from an empty queue */
    }
    /* Queue is not empty */
    else
    {
        /* Extract word from queue's front entry */
        taken_word = temp_ptr->array[temp_ptr->front];
        temp_ptr->front = (temp_ptr->front + 1U) % QUEUE_CAPACITY;
        temp_ptr->size--;
    }

    /* Issue assembly instruction that enables global interrupts */
    EXIT_CRITICAL_SECTION;

    return taken_word;
}


/* Function: checkQueueSize
 *
 * Description: Checks size of desired queue.
 *
 * Arguments:   queue - indicator of which queue is to be added to
 *
 * Returns:     unsigned short - number of entries in desired queue
 */
unsigned short checkQueueSize(enum Queues queue)
{
    struct Queue * temp_ptr;
    unsigned short size;

    /* Determine queue to be worked on */
    if(queue == QUEUE_COMMANDS)
    {
        temp_ptr = command_queue;
    }
    else if(queue == QUEUE_FEEDBACK)
    {
        temp_ptr = feedback_queue;
    }
    else
    {
        /* Should not get here so loop indefinitely to preserve
         * system state for debugging.
         */
        while(SET);
    }

    /* Issue assembly instruction that disables global interrupts */
    ENTER_CRITICAL_SECTION;

    /* Obtain size from appropriate queue */
    size = temp_ptr->size;

    /* Issue assembly instruction that enables global interrupts */
    EXIT_CRITICAL_SECTION;

    return size;
}
