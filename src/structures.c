/* File:        structures.c
 *
 * Author:      Patrick Wells
 *
 * Description: This source file initializes instances of the global data structures
 *              keeping track of essential information.
 *
 * Last Edited: 8 October 2019
 */


/* Included headers */
#include "structures.h"

/* Time variable that can be set/queried using commands.
 * This value starts at 00:00:00.0.
 */
extern struct time time = {0U, 0U, 0U, 0U};

/* Date variable that can be set/queried using commands.
 * This value starts at 1/Sept/2019. Note that the month index here
 * is actually one less than the month's index in the calendar year.
 */
extern struct date date = {1U, 8U, 2019U};

/* Alarm time variable used to trigger alarm set by commands.
 * This value starts at 00:00:00.0.
 */
extern struct time alarm_time = {0U, 0U, 0U, 0U};

/* Flag used to determine whether alarm is active. Alarm will be considered
 * active if this flag has a value of 1 and inactive if this flag has a value
 * of 0.
 */
extern unsigned char alarm_active = 0U;

/* Flag used to determine whether current year is a leap year. A value of 1
 * indicates a current leap year while a value of 0 indicates otherwise.
 */
extern unsigned char leap_year = 0U;
