/* File:        commandprocessing.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains prototypes of functions used in
 *              commandprocessing.c that are also called elsewhere in the project.
 *
 * Last Edited: 24 September 2019
 */

/* Wrapper ifdef */
#ifndef SRC_COMMANDPROCESSING_H_
#define SRC_COMMANDPROCESSING_H_


/* Function: commandTime
 *
 * Description: Sets/queries current time.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandTime(unsigned int * arg, unsigned char num_args);


/* Function: commandDate
 *
 * Description: Sets/queries current date.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandDate(unsigned int * arg, unsigned char num_args);


/* Function: commandAlarm
 *
 * Description: Sets/clears current alarm.
 *
 * Arguments:   arg      -  argument integers issued with command
 *              num_args - number of argument integers issued with command
 *
 * Returns:     void
 */
void commandAlarm(unsigned int * arg, unsigned char num_args);


/* Wrapper endif */
#endif /* SRC_COMMANDPROCESSING_H_ */
