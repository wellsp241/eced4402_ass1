/* File:        commanddecoding.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains prototypes of functions used in
 *              commanddecoding.c that are also called elsewhere in the project.
 *
 * Last Edited: 26 September 2019
 */

/* Wrapper ifdef */
#ifndef SRC_COMMANDDECODING_H_
#define SRC_COMMANDDECODING_H_


/* Function: decodeCommand
 *
 * Description: Determines which command has been received from user
 *
 * Arguments:   command - command string received
 *
 * Returns:     void
 */
void decodeCommand(char * command);


/* Wrapper endif */
#endif /* SRC_COMMANDDECODING_H_ */
