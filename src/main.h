/* File:        main.h
 *
 * Author:      Patrick Wells
 *
 * Description: This header file contains prototypes of functions used in
 *              main.c that are also called elsewhere in the project.
 *
 * Last Edited: 8 October 2019
 */

/* Wrapper ifdef */
#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_


/* Function:    SysTickHandler
 *
 * Description: ISR to be called once SYSTICK reaches zero.
 *              This should occur every tenth of a second.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void SysTickHandler(void);


/* Function:    UART0_IntHandler
 *
 * Description: ISR to be called once UART0 receives or transmits data.
 *              Queues/Dequeues commands/diagnostics to be sent/received
 *              through UART0.
 *
 * Arguments:   void
 *
 * Returns:     void
 */
void UART0_IntHandler(void);


/* Function:    startFeedbackTX
 *
 * Description: This function will start a UART transfer of command line feedback
 *              only if the dedicated UART is not already busy.
 *
 * Arguments:   ch - character to be transmitted
 *
 * Returns:     void
 */
unsigned char startFeedbackTX(char ch);


/* Wrapper endif */
#endif /* SRC_MAIN_H_ */
