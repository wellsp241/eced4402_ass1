################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/commanddecoding.c \
../src/commandprocessing.c \
../src/main.c \
../src/queue.c \
../src/structures.c \
../src/tm4c1294ncpdt_startup_ccs.c \
../src/utility.c 

C_DEPS += \
./src/commanddecoding.d \
./src/commandprocessing.d \
./src/main.d \
./src/queue.d \
./src/structures.d \
./src/tm4c1294ncpdt_startup_ccs.d \
./src/utility.d 

OBJS += \
./src/commanddecoding.obj \
./src/commandprocessing.obj \
./src/main.obj \
./src/queue.obj \
./src/structures.obj \
./src/tm4c1294ncpdt_startup_ccs.obj \
./src/utility.obj 

OBJS__QUOTED += \
"src\commanddecoding.obj" \
"src\commandprocessing.obj" \
"src\main.obj" \
"src\queue.obj" \
"src\structures.obj" \
"src\tm4c1294ncpdt_startup_ccs.obj" \
"src\utility.obj" 

C_DEPS__QUOTED += \
"src\commanddecoding.d" \
"src\commandprocessing.d" \
"src\main.d" \
"src\queue.d" \
"src\structures.d" \
"src\tm4c1294ncpdt_startup_ccs.d" \
"src\utility.d" 

C_SRCS__QUOTED += \
"../src/commanddecoding.c" \
"../src/commandprocessing.c" \
"../src/main.c" \
"../src/queue.c" \
"../src/structures.c" \
"../src/tm4c1294ncpdt_startup_ccs.c" \
"../src/utility.c" 


